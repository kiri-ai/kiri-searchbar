import React from "react";
import { Typography, Box, CardActionArea, Grid } from "@material-ui/core";
import KiriAgent, { QueryResult } from "kiri-search-library";

interface Props {
  result: QueryResult;
  agent: KiriAgent | undefined;
  resultClickHandler?: (url: string, article_id: string) => void;
}

function Result({ agent, result, resultClickHandler}: Props) {
  return (
    <Box m={2}>
      <CardActionArea
        onClick={() => {
          if (!agent) {
            return;
          }
          if(resultClickHandler){
            resultClickHandler(result.url, result.id);
          }else {
            agent.redirect(result.url, result.id);
          }
        }}
      >
        <Grid container>
          <Grid item>
            <Typography variant="h6">{result.title}</Typography>
          </Grid>
        </Grid>

        <Typography variant="body2">{result.answer}</Typography>
      </CardActionArea>
    </Box>
  );
}

export default Result;
