import React from "react";
import Result from "./Result";
import { Card, Box, Divider, CardContent, Typography } from "@material-ui/core";
import KiriLogo from "./KiriLogo";
import KiriAgent, { QueryResult } from "kiri-search-library";

interface Props {
  results: QueryResult[];
  agent: KiriAgent | undefined;
  query: string;
  resultClickHandler?: (url: string, article_id: string) => void;
}

function Results({ results, agent, query, resultClickHandler }: Props) {
  const resultList = (
    <div className="text-left">
      {results.map(function (object: QueryResult) {
        return <Result result={object} agent={agent} resultClickHandler={resultClickHandler} />;
      })}
    </div>
  );

  return (
    <div>
      {query && results.length !== 0 && (
        <Card
          style={{
            maxWidth: "400px",
            marginTop: 5,
            position: "absolute",
            zIndex: 10,
          }}
        >
          <CardContent style={{ paddingBottom: "8px" }}>
            <Typography gutterBottom variant="subtitle1" component="p">
              Results
            </Typography>
            <Divider />
            {resultList}
            <Box
              style={{ marginLeft: "auto", marginRight: 0, textAlign: "right" }}
            >
              <Typography variant="caption">Powered by </Typography>
              <a href="https://kiri.ai" >
                <KiriLogo></KiriLogo>
              </a>
            </Box>
          </CardContent>
        </Card>
      )}
    </div>
  );
}

export default Results;
