import React, { useEffect, useState } from "react";
import { Box } from "@material-ui/core";
import Results from "./Results";
import KiriAgent, { Filters, QueryResult } from "kiri-search-library";
import styles from "./index.css";

interface Props {
  apiKey: string;
  numResults: number;
  pageSize?: number;
  placeholder?: string;
  filters?: Filters;
  pageNumber?: number;
  onEnterHandler?: (results: QueryResult[], totalResults: number, totalPages: number) => void;
  resultClickHandler?: (url: string, article_id: string) => void;
}

function Search({onEnterHandler, resultClickHandler, ...props} : Props) {
  const [query, setQuery] = useState("");
  const [results, setResults] = useState<QueryResult[]>([]);
  const [totalResults, setTotal] = useState<number>(0);
  const [totalPages, setPages] = useState<number>(0);
  const [visible, setVisible] = useState(false);
  const [agent, setAgent] = useState<KiriAgent>();

  useEffect(() => {
    if (!agent) {
      setAgent(new KiriAgent(props.apiKey));
    }
  }, [agent, props.apiKey]);

  async function search(q: string, n: number, f?: Filters, p?: number) {
    setQuery(q);
    if (q.length === 0){
      return;
    }

    try {
      if (!agent) {
        return;
      }
      const response = await agent.search(q, n, f, p);

      if (!response) {
        alert("There was a problem getting search results");
        return;
      }
      setTotal(response.totalResults);
      setPages(response.totalPages);
      setResults(response.results);
    } catch (e) {
      console.log("Trouble getting search results");
    }
  }

  return (
    <Box textAlign="left">
      <div className={styles.custom_input_group}>
        <input
          className={styles.form_style}
          type="text"
          placeholder={props.placeholder}
          onChange={(event) => search(event.target.value, props.numResults, props.filters )}
          onFocus={() => setVisible(true)}
          onBlur={(e) =>
            // Hide only for non link clicks
            {
              if (e.relatedTarget == null) {
                setVisible(false);
              }
            }
          }
          onKeyUp={(e) => {
            if (e.key === "Enter"){
              if (!onEnterHandler || !props.pageSize){
                return;
              }

              search(query, props.pageSize)
              onEnterHandler(results, totalResults, totalPages);
            }
          }}
          autoComplete="off"
        />
      </div>
      {visible && <Results results={results} query={query} agent={agent} resultClickHandler={resultClickHandler} />}
    </Box>
  );
}
export default Search;
