## Kiri searchbar

> A React component for the kiri-search-library javascript package. Displays a search bar, which calls the Search API for every key pressed. The search results are displayed immediately as clickable cards.

[![NPM](https://img.shields.io/npm/v/kiri-search-bar.svg)](https://www.npmjs.com/package/kiri-search-bar) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

## Install

```bash
npm install --save kiri-search-bar
```

## Usage

### React

```tsx
import React, { Component } from "react";

import Search from "kiri-search-bar";
// Import this for default styling
import "kiri-search-bar/dist/index.css";
import { Filters } from "kiri-search-library";

// Optional
const filters = { Language: "" } as Filters;

class Example extends Component {
  render() {
    return <Search apiKey="your_key" numResults={3} filters={filters} />;
  }
}
```

### CDN (UMD)

```html
<html>
  <meta charset="UTF-8" />
  <head>
    <!-- Note: when deploying, replace "development.js" with "production.min.js". -->
    <!-- Load React. -->
    <script
      src="https://unpkg.com/react@16/umd/react.development.js"
      crossorigin
    ></script>
    <script
      src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"
      crossorigin
    ></script>
    <!-- Load Material UI. -->
    <script src="https://unpkg.com/@material-ui/core@4.11.0/umd/material-ui.development.js"></script>

    <!-- Search Library -->
    <script src="https://unpkg.com/kiri-search-library@1.0.17/src/kiri.js"></script>
    <!-- Search Bar -->
    <script src="https://unpkg.com/kiri-search-bar@0.1.8/dist/index.umd.js"></script>

    <!-- Default Searchbar Styling. -->
    <link
      rel="stylesheet"
      href="https://unpkg.com/kiri-search-bar@0.1.8/dist/index.css"
    />
  </head>
  <body>
    <!-- Container where Search bar will be located. -->
    <div id="search_container"></div>

    <script>
      window.onload = () => {
        const domContainer = document.querySelector("#search_container");
        ReactDOM.render(
          React.createElement(Search, {
            apiKey: "your_api_key",
            numResults: 1,
            filters: { Language: "English" }, // Optional
          }),
          domContainer
        );
      };
    </script>
  </body>
</html>
```

## Search Component Props

### `apiKey`

The apiKey linked to your account.

### `numResults`

The number of results to return in the result list.

### `filters`

Returns the articles that satisfy the provided filters.

- **Language (string)**: The language of articles to return.

### `resultClickHandler`

A callback function that is called when a result is clicked

- **_params_**
  - **url (string)**: The url linked to the article.
  - **article_id (string)**: The article id of the article that was clicked.

## License

Apache Version 2.0 © [Kiri AI](https://kiri.ai)
